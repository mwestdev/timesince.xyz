# Generated by Django 2.2.4 on 2019-08-29 14:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timers', '0002_auto_20190829_1432'),
    ]

    operations = [
        migrations.AlterField(
            model_name='timer',
            name='private',
            field=models.BooleanField(default=False),
        ),
    ]
