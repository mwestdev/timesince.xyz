# Generated by Django 2.2.4 on 2019-09-06 01:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timers', '0004_auto_20190905_1611'),
    ]

    operations = [
        migrations.AlterField(
            model_name='timer',
            name='give_up_date',
            field=models.DateTimeField(),
        ),
    ]
