from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    ListView,
    UpdateView,
)

from .forms import TimerForm
from .models import Timer


class TimerListView(ListView):
    model = Timer
    template_name = "timers/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["timers"] = Timer.objects.filter(private=False).order_by("-date_posted")
        paginator2 = Paginator(context["timers"], 8)
        page2 = self.request.GET.get("timers_page")
        try:
            context["timers"] = paginator2.page(page2)
        except PageNotAnInteger:
            context["timers"] = paginator2.page(1)
        except EmptyPage:
            context["timers"] = paginator2.page(paginator2.num_pages)

        return context


class UserTimerListView(ListView):
    model = Timer
    template_name = "timers/user_timers.html"

    def get_context_data(self, **kwargs):
        context = super(UserTimerListView, self).get_context_data(**kwargs)
        user = get_object_or_404(User, username=self.kwargs.get("username"))

        # Only display private timers to their author
        if user == self.request.user:
            context["timers"] = Timer.objects.filter(author=user).order_by(
                "-date_posted"
            )
        else:
            context["timers"] = Timer.objects.filter(
                author=user, private=False
            ).order_by("-date_posted")

        paginator2 = Paginator(context["timers"], 8)
        page2 = self.request.GET.get("timers_page")
        try:
            context["timers"] = paginator2.page(page2)
        except PageNotAnInteger:
            context["timers"] = paginator2.page(1)
        except EmptyPage:
            context["timers"] = paginator2.page(paginator2.num_pages)

        return context


class TimerDetailView(DetailView):
    model = Timer


class TimerCreateView(LoginRequiredMixin, CreateView):
    model = Timer
    form_class = TimerForm

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        form = super(TimerCreateView, self).get_form(form_class)
        form.fields["title"].widget.attrs = {"placeholder": "Title"}
        form.fields["title"].label = False
        form.fields["give_up_date"].label = False
        form.fields["give_up_date"].input_formats = ("%b %d, %Y %I:%M %p",)
        return form


class TimerUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Timer
    form_class = TimerForm

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        timer = self.get_object()
        if self.request.user == timer.author:
            return True
        return False

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        form = super(TimerUpdateView, self).get_form(form_class)
        form.fields["title"].widget.attrs = {"placeholder": "Title"}
        form.fields["title"].label = False
        form.fields["give_up_date"].label = False
        form.fields["give_up_date"].input_formats = ("%b %d, %Y %I:%M %p",)
        return form


class TimerDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Timer

    def get_success_url(self):
        return reverse("profile", kwargs={"username": self.request.user})

    def test_func(self):
        timer = self.get_object()
        if self.request.user == timer.author:
            return True
        return False
