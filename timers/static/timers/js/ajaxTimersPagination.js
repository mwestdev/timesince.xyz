function ajaxTimersPagination() {
    $("#timers-pagination a.page-link").each((index, el) => {
        $(el).click(e => {
            e.preventDefault();
            let page_url = $(el).attr("href");
            console.log(page_url);

            $.ajax({
                url: page_url,
                type: "GET",
                success: data => {
                    $("#timers").fadeOut(500, function() {
                        $(this).empty();
                        $(this)
                            .append(
                                $(data)
                                    .find("#timers")
                                    .html()
                            )
                            .fadeIn(500);
                    });

                    $("#timers-pagination").empty();
                    $("#timers-pagination").append(
                        $(data)
                            .find("#timers-pagination")
                            .html()
                    );
                },
            });
        });
    });
}

$(document).ready(function() {
    ajaxTimersPagination();
});
$(document).ajaxStop(function() {
    ajaxTimersPagination();
});
