from django.forms import ModelForm
from tempus_dominus.widgets import DateTimePicker

from .models import Timer


class TimerForm(ModelForm):
    class Meta:
        model = Timer
        fields = ["title", "give_up_date", "private"]
        widgets = {
            "give_up_date": DateTimePicker(
                options={
                    "useCurrent": False,
                    "collapse": True,
                    "format": "lll",
                    "maxDate": "now",
                },
                attrs={
                    "append": "fa fa-calendar",
                    "icon_toggle": True,
                    "placeholder": "Date & Time",
                },
            )
        }
