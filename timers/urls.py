from django.urls import path

from .views import (
    TimerCreateView,
    TimerDeleteView,
    TimerDetailView,
    TimerListView,
    TimerUpdateView,
    UserTimerListView,
)

urlpatterns = [
    path("", TimerListView.as_view(), name="timers-home"),
    path("user/<str:username>", UserTimerListView.as_view(), name="user-timers"),
    path("timer/<int:pk>/", TimerDetailView.as_view(), name="timer-detail"),
    path("timer/new/", TimerCreateView.as_view(), name="timer-create"),
    path("timer/<int:pk>/update/", TimerUpdateView.as_view(), name="timer-update"),
    path("timer/<int:pk>/delete/", TimerDeleteView.as_view(), name="timer-delete"),
]
