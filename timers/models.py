from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils import timezone


class Timer(models.Model):
    title = models.CharField(max_length=100)
    date_posted = models.DateTimeField(default=timezone.now)
    give_up_date = models.DateTimeField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    private = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("timer-detail", kwargs={"pk": self.pk})
