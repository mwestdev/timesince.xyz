from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from main import views as main_views

urlpatterns = [
    path("", include("main.urls")),
    path("", include("users.urls")),
    path("timers/", include("timers.urls")),
    path("posts/", include("posts.urls")),
    path("admin/", admin.site.urls),
]

# Custom error pages
handler403 = main_views.error_403
handler404 = main_views.error_404
handler429 = main_views.error_429
handler500 = main_views.error_500

# Debug urls
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    import debug_toolbar

    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
