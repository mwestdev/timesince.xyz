import json
import os

with open("/etc/timesince-config.json") as config_file:
    config = json.load(config_file)

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config.get("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config.get("DEBUG")

ALLOWED_HOSTS = config.get("ALLOWED_HOSTS")

INTERNAL_IPS = ["127.0.0.1"]

# Application definition
INSTALLED_APPS = [
    "main.apps.MainConfig",
    "posts.apps.PostsConfig",
    "users.apps.UsersConfig",
    "timers.apps.TimersConfig",
    "crispy_forms",
    "pipeline",
    "debug_toolbar",
    "tempus_dominus",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.humanize",
]

MIDDLEWARE = [
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "timesince.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

WSGI_APPLICATION = "timesince.wsgi.application"

# Database
DATABASES = {
    "default": {
        "ENGINE": config.get("DATABASE_ENGINE"),
        "NAME": config.get("DATABASE_NAME"),
        "USER": config.get("DATABASE_USER"),
        "PASSWORD": config.get("DATABASE_PASSWORD"),
        "HOST": config.get("DATABASE_HOST"),
        "PORT": config.get("DATABASE_PORT"),
    }
}

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

# Internationalization
LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

# PIPELINE CONFIG
PIPELINE = {
    "STYLESHEETS": {
        "styles": {
            "source_filenames": (
                "main/styles/main.scss",
                "timers/styles/timers.scss",
                "posts/styles/posts.scss",
                "users/styles/users.scss",
            ),
            "output_filename": "styles/main.min.css",
        }
    },
    "JAVASCRIPT": {
        "scripts": {
            "source_filenames": (
                "main/js/navbar.js",
                "timers/js/ajaxTimersPagination.js",
                "posts/js/ajaxPostsPagination.js",
                "posts/js/commentTabs.js",
            ),
            "output_filename": "js/main.min.js",
        }
    },
    "CSS_COMPRESSOR": "pipeline.compressors.cssmin.CSSMinCompressor",
    "JS_COMPRESSOR": "pipeline.compressors.jsmin.JSMinCompressor",
    "SASS_BINARY": os.path.join(BASE_DIR, "venv/bin/sassc"),
    "COMPILERS": ("pipeline.compilers.sass.SASSCompiler",),
}

STATICFILES_STORAGE = "pipeline.storage.PipelineCachedStorage"

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "pipeline.finders.PipelineFinder",
)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/
STATIC_ROOT = os.path.join(BASE_DIR, "static")
STATIC_URL = "/static/"


MEDIA_ROOT = os.path.join(BASE_DIR, "media")
MEDIA_URL = "/media/"

CRISPY_TEMPLATE_PACK = "bootstrap4"

LOGIN_REDIRECT_URL = "home"
LOGIN_URL = "login"

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = "smtp.gmail.com"
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_USER = config.get("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = config.get("EMAIL_HOST_PASSWORD")
