from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from .forms import CommentForm
from .models import Comment, Post


class PostListView(ListView):
    model = Post
    template_name = "posts/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["posts"] = Post.objects.filter(private=False).order_by("-date_posted")
        paginator2 = Paginator(context["posts"], 6)
        page2 = self.request.GET.get("posts_page")
        try:
            context["posts"] = paginator2.page(page2)
        except PageNotAnInteger:
            context["posts"] = paginator2.page(1)
        except EmptyPage:
            context["posts"] = paginator2.page(paginator2.num_pages)

        return context


class UserPostListView(ListView):
    model = Post
    template_name = "posts/user_posts.html"  # <app>/<model>_<viewtype>.html

    def get_context_data(self, **kwargs):
        context = super(UserPostListView, self).get_context_data(**kwargs)
        user = get_object_or_404(User, username=self.kwargs.get("username"))

        # Only display private posts to their author
        if user == self.request.user:
            context["posts"] = Post.objects.filter(author=user).order_by("-date_posted")
        else:
            context["posts"] = Post.objects.filter(author=user, private=False).order_by(
                "-date_posted"
            )

        paginator2 = Paginator(context["posts"], 6)
        page2 = self.request.GET.get("posts_page")
        try:
            context["posts"] = paginator2.page(page2)
        except PageNotAnInteger:
            context["posts"] = paginator2.page(1)
        except EmptyPage:
            context["posts"] = paginator2.page(paginator2.num_pages)

        return context


def post_detail(request, pk):
    post = Post.objects.get(pk=pk)

    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = Comment(
                author=request.user, content=form.cleaned_data["content"], post=post
            )
            comment.save()
            return redirect("post-detail", pk=pk)

    else:
        form = CommentForm()

    comments = Comment.objects.filter(post=post)
    context = {"post": post, "comments": comments, "form": form}

    return render(request, "posts/post_detail.html", context)


class PostCreateView(LoginRequiredMixin, CreateView):
    model = Post
    fields = ["title", "content", "private"]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        form = super(PostCreateView, self).get_form(form_class)
        form.fields["title"].widget.attrs = {"placeholder": "Title"}
        form.fields["title"].label = False
        form.fields["content"].widget.attrs = {"placeholder": "Content", "rows": "10"}
        form.fields["content"].label = False
        return form


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Post
    fields = ["title", "content", "private"]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        form = super(PostUpdateView, self).get_form(form_class)
        form.fields["title"].widget.attrs = {"placeholder": "Title"}
        form.fields["title"].label = False
        form.fields["content"].widget.attrs = {"placeholder": "Content", "rows": "10"}
        form.fields["content"].label = False
        return form


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post

    def get_success_url(self):
        return reverse("profile", kwargs={"username": self.request.user})

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


class CommentDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Comment

    def get_success_url(self):
        comment = self.get_object()
        return reverse("post-detail", kwargs={"pk": comment.post.id})

    def test_func(self):
        comment = self.get_object()
        if self.request.user == comment.author:
            return True
        return False


class CommentUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Comment
    fields = ["content"]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        comment = self.get_object()
        if self.request.user == comment.author:
            return True
        return False

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()

        form = super(CommentUpdateView, self).get_form(form_class)
        form.fields["content"].widget.attrs = {"placeholder": "Content", "rows": "5"}
        form.fields["content"].label = False
        return form
