function ajaxPostsPagination() {
    $("#posts-pagination a.page-link").each((index, el) => {
        $(el).click(e => {
            e.preventDefault();
            let page_url = $(el).attr("href");
            console.log(page_url);

            $.ajax({
                url: page_url,
                type: "GET",
                success: data => {
                    $("#posts").fadeOut(500, function() {
                        $(this).empty();
                        $(this)
                            .append(
                                $(data)
                                    .find("#posts")
                                    .html()
                            )
                            .fadeIn(500);
                    });

                    $("#posts-pagination").empty();
                    $("#posts-pagination").append(
                        $(data)
                            .find("#posts-pagination")
                            .html()
                    );
                },
            });
        });
    });
}

$(document).ready(function() {
    ajaxPostsPagination();
});
$(document).ajaxStop(function() {
    ajaxPostsPagination();
});
