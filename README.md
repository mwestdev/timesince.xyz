# Public repository for my website: [timesince.xyz](https://timesince.xyz) powered by Django.

### Clone this repo:
```
git clone https://gitlab.com/mwestdev/timesince.xyz.git ~/timesince.xyz
```

### Set up python virtual environment:
```
cd ~/timesince.xyz
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
### Create config file:
* For our development environment we will use an sqlite db, the blank options such as HOST, PORT etc can be filled out in production if you are using postgres.
* EMAIL_HOST_USER and EMAIL_HOST_PASS are the login credentials for your email account that will be resposible for sending out password reset emails, for development I just use a simple free gmail account.
* Generate a strong secret key and ensure debug is set to false in production.
* When DEBUG is True and ALLOWED_HOSTS is empty, the host is validated against ['localhost', '127.0.0.1', '[::1]'].
```
sudo touch /etc/timesince-config.json
sudo vi /etc/timesince-config.json
```

```json
{
    "SECRET_KEY":"SECRET_KEY_HERE",
    "ALLOWED_HOSTS":[],
    "DEBUG":true,
    "DATABASE_ENGINE":"django.db.backends.sqlite3",
    "DATABASE_NAME":"db.sqlite3",
    "DATABASE_USER":"",
    "DATABASE_PASSWORD":"",
    "DATABASE_HOST":"",
    "DATABASE_PORT":"",
    "EMAIL_HOST_USER":"EMAIL_HOST_USER_HERE",
    "EMAIL_HOST_PASSWORD":"EMAIL_HOST_PASSWORD_HERE"
}
```

### Install npm dependencies:
```
cd ~/timesince.xyz
npm install
```

### Run this app locally:
```
cd ~/timesince.xyz
source venv/bin/activate
python3 manage.py runserver
```

### App should now be running at:

```
http://localhost:8000/
```

### Create pre-commit hook file:
```
cd ~/timesince.xyz/.git/hooks
touch pre-commit
vi pre-commit
```
### Update requirements.txt and migrate database before each commit:
```sh
#!/bin/sh
set -eu

PROJECT_NAME="timesince.xyz"
TARGET="$HOME/$PROJECT_NAME"

# Activate virtual env
source "$TARGET"/venv/bin/activate

# Update requirements.txt
pip freeze > "$TARGET"/requirements.txt

# Migrate DB
python3 "$TARGET"/manage.py makemigrations
python3 "$TARGET"/manage.py migrate

# Add changes to commit
git add -A
```
