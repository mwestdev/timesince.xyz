from django import forms
from django.contrib.auth.forms import (
    AuthenticationForm,
    PasswordChangeForm,
    PasswordResetForm,
    SetPasswordForm,
    UserCreationForm,
)
from django.contrib.auth.models import User

from .models import Setting


class CustomAuthForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ["username", "password"]

    def __init__(self, *args, **kwargs):
        super(CustomAuthForm, self).__init__(*args, **kwargs)
        self.fields["username"].widget.attrs.update({"placeholder": "Username"})
        self.fields["username"].label = False
        self.fields["password"].widget.attrs.update({"placeholder": "Password"})
        self.fields["password"].label = False


class CustomResetForm(PasswordResetForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ["email"]

    def __init__(self, *args, **kwargs):
        super(CustomResetForm, self).__init__(*args, **kwargs)
        self.fields["email"].widget.attrs.update({"placeholder": "Email"})
        self.fields["email"].label = False


class CustomResetConfirmForm(SetPasswordForm):
    class Meta:
        model = User
        fields = ["new_password1", "new_password2"]

    def __init__(self, *args, **kwargs):
        super(CustomResetConfirmForm, self).__init__(*args, **kwargs)
        self.fields["new_password1"].widget.attrs.update(
            {"placeholder": "New password"}
        )
        self.fields["new_password1"].label = False
        self.fields["new_password1"].help_text = False
        self.fields["new_password2"].widget.attrs.update(
            {"placeholder": "New password confirm"}
        )
        self.fields["new_password2"].label = False


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ["username", "email", "password1", "password2"]

    def __init__(self, *args, **kwargs):
        super(UserRegisterForm, self).__init__(*args, **kwargs)
        self.fields["username"].widget.attrs.update({"placeholder": "Username"})
        self.fields["username"].label = False
        self.fields["username"].help_text = False
        self.fields["email"].widget.attrs.update({"placeholder": "Email"})
        self.fields["email"].label = False
        self.fields["email"].help_text = False
        self.fields["password1"].widget.attrs.update({"placeholder": "Password"})
        self.fields["password1"].label = False
        self.fields["password1"].help_text = False
        self.fields["password2"].widget.attrs.update(
            {"placeholder": "Password Confirm"}
        )
        self.fields["password2"].label = False
        self.fields["password2"].help_text = False


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ["username", "email"]

    def __init__(self, *args, **kwargs):
        super(UserUpdateForm, self).__init__(*args, **kwargs)
        self.fields["username"].widget.attrs.update(
            {"placeholder": "Username", "class": "form-control-lg"}
        )
        self.fields["username"].help_text = False
        self.fields["email"].widget.attrs.update(
            {"placeholder": "Email", "class": "form-control-lg"}
        )


class PictureUpdateForm(forms.ModelForm):
    class Meta:
        model = Setting
        fields = ["image"]

    def __init__(self, *args, **kwargs):
        super(PictureUpdateForm, self).__init__(*args, **kwargs)
        self.fields["image"].label = "Profile Picture"
        self.fields["image"].required = False
        self.fields["image"].widget = forms.FileInput()


class CustomPasswordForm(PasswordChangeForm):
    class Meta:
        model = User
        fields = ["old_password", "new_password1", "new_password2"]

    def __init__(self, *args, **kwargs):
        super(CustomPasswordForm, self).__init__(*args, **kwargs)
        self.fields["old_password"].widget.attrs.update({"placeholder": "Old Password"})
        self.fields["old_password"].label = False
        self.fields["new_password1"].widget.attrs.update(
            {"placeholder": "New Password"}
        )
        self.fields["new_password1"].label = False
        self.fields["new_password1"].help_text = False
        self.fields["new_password2"].widget.attrs.update(
            {"placeholder": "New Password Confirm"}
        )
        self.fields["new_password2"].label = False
