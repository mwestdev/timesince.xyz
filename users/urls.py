from django.contrib.auth import views as auth_views
from django.urls import path

from . import views as user_views
from .forms import CustomAuthForm, CustomResetConfirmForm, CustomResetForm
from .views import ProfileListView

urlpatterns = [
    path("register/", user_views.register, name="register"),
    path("profile/<str:username>", ProfileListView.as_view(), name="profile"),
    path("settings/", user_views.settings, name="settings"),
    path(
        "login/",
        auth_views.LoginView.as_view(
            template_name="users/login.html", authentication_form=CustomAuthForm
        ),
        name="login",
    ),
    path(
        "logout/",
        auth_views.LogoutView.as_view(template_name="users/logout.html"),
        name="logout",
    ),
    path(
        "password-reset/",
        auth_views.PasswordResetView.as_view(
            template_name="users/password_reset.html", form_class=CustomResetForm
        ),
        name="password_reset",
    ),
    path(
        "password-reset/done/",
        auth_views.PasswordResetDoneView.as_view(
            template_name="users/password_reset_done.html"
        ),
        name="password_reset_done",
    ),
    path(
        "password-reset-confirm/<uidb64>/<token>/",
        auth_views.PasswordResetConfirmView.as_view(
            template_name="users/password_reset_confirm.html",
            form_class=CustomResetConfirmForm,
        ),
        name="password_reset_confirm",
    ),
    path(
        "password-reset-complete/",
        auth_views.PasswordResetCompleteView.as_view(
            template_name="users/password_reset_complete.html"
        ),
        name="password_reset_complete",
    ),
    path("change-password/", user_views.change_password, name="change_password"),
]
