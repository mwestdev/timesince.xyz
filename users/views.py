from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic import ListView
from posts.models import Post
from timers.models import Timer

from .forms import (
    CustomPasswordForm,
    PictureUpdateForm,
    UserRegisterForm,
    UserUpdateForm,
)


def register(request):
    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(
                request, f"Your account has been created! You are now able to log in"
            )
            return redirect("login")
    else:
        form = UserRegisterForm()
    return render(request, "users/register.html", {"form": form})


@login_required
def settings(request):
    if request.method == "POST":
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = PictureUpdateForm(
            request.POST, request.FILES, instance=request.user.setting
        )
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f"Your profile has been updated!")
            return redirect("settings")

    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = PictureUpdateForm(instance=request.user.setting)

    context = {"u_form": u_form, "p_form": p_form}

    return render(request, "users/settings.html", context)


@login_required
def change_password(request):
    if request.method == "POST":
        form = CustomPasswordForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, "Your password was successfully updated!")
            return redirect("change_password")
    else:
        form = CustomPasswordForm(request.user)
    return render(request, "users/change_password.html", {"form": form})


class ProfileListView(ListView):
    model = Timer
    template_name = "users/profile.html"

    def get_context_data(self, **kwargs):
        context = super(ProfileListView, self).get_context_data(**kwargs)
        user = get_object_or_404(User, username=self.kwargs.get("username"))

        # Only display private timers/posts to their author
        if user == self.request.user:
            context["timers"] = Timer.objects.filter(author=user).order_by(
                "-date_posted"
            )
            context["posts"] = Post.objects.filter(author=user).order_by("-date_posted")
        else:
            context["timers"] = Timer.objects.filter(
                author=user, private=False
            ).order_by("-date_posted")
            context["posts"] = Post.objects.filter(author=user, private=False).order_by(
                "-date_posted"
            )

        paginator1 = Paginator(context["timers"], 4)
        page1 = self.request.GET.get("timers_page")
        try:
            context["timers"] = paginator1.page(page1)
        except PageNotAnInteger:
            context["timers"] = paginator1.page(1)
        except EmptyPage:
            context["timers"] = paginator1.page(paginator1.num_pages)

        paginator2 = Paginator(context["posts"], 2)
        page2 = self.request.GET.get("posts_page")
        try:
            context["posts"] = paginator2.page(page2)
        except PageNotAnInteger:
            context["posts"] = paginator2.page(1)
        except EmptyPage:
            context["posts"] = paginator2.page(paginator2.num_pages)

        return context
