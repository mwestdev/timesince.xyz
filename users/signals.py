from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Setting


@receiver(post_save, sender=User)
def create_setting(sender, instance, created, **kwargs):
    if created:
        Setting.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_setting(sender, instance, **kwargs):
    instance.setting.save()
