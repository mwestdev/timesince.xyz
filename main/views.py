from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.shortcuts import render
from django.views.generic import ListView
from posts.models import Post
from timers.models import Timer


def error_403(request, exception=None):
    return render(request, "main/errors/403.html")


def error_404(request, exception=None):
    return render(request, "main/errors/404.html")


def error_429(request, exception=None):
    return render(request, "main/errors/429.html")


def error_500(request):
    return render(request, "main/errors/500.html")


class HomeListView(ListView):
    model = Timer
    template_name = "main/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["timers"] = Timer.objects.filter(private=False).order_by("-date_posted")
        paginator1 = Paginator(context["timers"], 4)
        page1 = self.request.GET.get("timers_page")
        try:
            context["timers"] = paginator1.page(page1)
        except PageNotAnInteger:
            context["timers"] = paginator1.page(1)
        except EmptyPage:
            context["timers"] = paginator1.page(paginator1.num_pages)

        context["posts"] = Post.objects.filter(private=False).order_by("-date_posted")
        paginator2 = Paginator(context["posts"], 2)
        page2 = self.request.GET.get("posts_page")
        try:
            context["posts"] = paginator2.page(page2)
        except PageNotAnInteger:
            context["posts"] = paginator2.page(1)
        except EmptyPage:
            context["posts"] = paginator2.page(paginator2.num_pages)

        return context
