$(document).ready(function() {
    // Highlight current page on navbar

    var url = window.location;

    $(".navbar-nav a")
        .filter(function() {
            return this.href == url;
        })
        .addClass("active");
});
